package com.konverz.human.interaction.service.utils;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

@Service
public class ServiceCaller {

	@Autowired
	private RestTemplate restTemplate;
	
	public ResponseEntity<JsonNode> post(String url, String body) throws RestClientException, URISyntaxException {
		return restTemplate.postForEntity(new URI(url), body, JsonNode.class);
	}
	
}
