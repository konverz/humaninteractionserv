package com.konverz.human.interaction.service.domain.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.konverz.human.interaction.service.domain.VoiceInteractionService;
import com.konverz.human.interaction.service.dto.VoiceCommandMessage;
import com.konverz.human.interaction.service.dto.VoiceMessage;

@Service
public class DefaultVoiceInteractionService implements VoiceInteractionService {

	@Autowired
	private ObjectMapper mapper;
	
	public void publishMessagesToTopic(VoiceCommandMessage message)
			throws Exception {
		mapper.writeValueAsString(message);
	}

	public VoiceMessage pollMessagesForTopic(String userId) throws Exception {
		VoiceMessage message = new VoiceMessage();
		message.setText("Hello there! I shall grant you 3 wishes.");
		return message;
	}

	
}
