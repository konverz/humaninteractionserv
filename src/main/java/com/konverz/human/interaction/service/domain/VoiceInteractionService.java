package com.konverz.human.interaction.service.domain;

import org.springframework.stereotype.Service;

import com.konverz.human.interaction.service.dto.VoiceCommandMessage;
import com.konverz.human.interaction.service.dto.VoiceMessage;

@Service
public interface VoiceInteractionService {

	void publishMessagesToTopic(VoiceCommandMessage message) throws Exception;
	
	VoiceMessage pollMessagesForTopic(String userId) throws Exception;
	
}
