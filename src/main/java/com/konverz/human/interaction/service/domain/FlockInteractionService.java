package com.konverz.human.interaction.service.domain;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.konverz.human.interaction.service.dto.FlockIncomingWebhook;

@Service
public interface FlockInteractionService {

	public enum EVENT_TYPE {
		APP_INSTALL,
		SLASH_COMMAND
	};
	
	EVENT_TYPE getEventType(JsonNode response);
	
	void handleAppInstall(JsonNode response) throws Exception;
	
	void handleNotification(String webhookId, FlockIncomingWebhook message) throws Exception;
	
	void handleIncomingMessage(JsonNode response) throws Exception;
	
}
