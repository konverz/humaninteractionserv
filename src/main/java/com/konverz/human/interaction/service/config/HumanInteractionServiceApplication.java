package com.konverz.human.interaction.service.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootApplication
@ComponentScan({
		"com.konverz.human.interaction.service.controller",
		"com.konverz.human.interaction.service.domain",
		"com.konverz.human.interaction.service.controller",
		"com.konverz.human.interaction.service.utils"
})
@Configuration
public class HumanInteractionServiceApplication {

	public static void main(String args[]) {
		SpringApplication.run(HumanInteractionServiceApplication.class, args);
	}
	
	@Bean
	public ObjectMapper objectMapper() {
		return new ObjectMapper();
	}
	
	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
