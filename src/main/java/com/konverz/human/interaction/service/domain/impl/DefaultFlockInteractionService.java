package com.konverz.human.interaction.service.domain.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konverz.human.interaction.service.domain.FlockInteractionService;
import com.konverz.human.interaction.service.dto.FlockIncomingWebhook;
import com.konverz.human.interaction.service.utils.ServiceCaller;

@Service
public class DefaultFlockInteractionService implements FlockInteractionService {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private static final String WEBHOOK_BASE_URL = "https://api.flock.co/hooks/sendMessage/";
	
	@Value("${com.konverz.service-discovery.user-service-url}")
	private String userService;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	private ServiceCaller caller;
	
	public EVENT_TYPE getEventType(JsonNode response) {
		String eventType = response.get("name").textValue();
		
		if (eventType.equals("app.install")) 
			return EVENT_TYPE.APP_INSTALL;
		else if (eventType.equals("client.slashCommand"))
			return EVENT_TYPE.SLASH_COMMAND;
		else
			throw new IllegalArgumentException("Received unknown event:" + eventType);
	}

	public void handleAppInstall(JsonNode response) throws Exception {
		logger.info(response.toString());
		
		/*
		FlockAppInstallResponse userInfo = mapper.treeToValue(response, FlockAppInstallResponse.class);
		caller.post(userService, mapper.writeValueAsString(userInfo));
		*/
	}

	public void handleNotification(String webhookId, FlockIncomingWebhook message) throws Exception {
		caller.post(WEBHOOK_BASE_URL + webhookId, mapper.writeValueAsString(message));
	}

	public void handleIncomingMessage(JsonNode response) throws Exception {
		logger.info(response.toString());
	}

}
