package com.konverz.human.interaction.service.dto;

public class VoiceMessage {

	private String text;

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}
