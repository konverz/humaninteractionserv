package com.konverz.human.interaction.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VoiceCommandMessage {
	
	@JsonProperty("user_id")
	private String userId;
	
	private String text;
	
	private String place;
	
	private String activity;
	
	private Time time;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}

	public static class Time {
		
		private String hour;
		
		private String minute;

		public String getHour() {
			return hour;
		}

		public void setHour(String hour) {
			this.hour = hour;
		}

		public String getMinute() {
			return minute;
		}

		public void setMinute(String minute) {
			this.minute = minute;
		}
		
	}

}
