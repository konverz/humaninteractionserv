package com.konverz.human.interaction.service.dto;

public class FlockAppInstallResponse {

	private String token;
	
	private String userId;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	
}
