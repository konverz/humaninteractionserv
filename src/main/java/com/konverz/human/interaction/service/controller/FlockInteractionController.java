package com.konverz.human.interaction.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.konverz.human.interaction.service.domain.FlockInteractionService;
import com.konverz.human.interaction.service.dto.FlockIncomingWebhook;

@RestController
@RequestMapping(value = "/flock", produces = MediaType.APPLICATION_JSON_VALUE)
public class FlockInteractionController {

	@Autowired
	private FlockInteractionService service;
	
	@Autowired
	private ObjectMapper mapper;
	
	@RequestMapping(value = "/events", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String handleFlockEvents(@RequestBody final JsonNode flockResponse) throws Exception {
		switch (service.getEventType(flockResponse)) {
		case APP_INSTALL:
			service.handleAppInstall(flockResponse);
			break;
		case SLASH_COMMAND:
			break;
		}
		
		return "";
	}
	
	@RequestMapping(value = "/configure", method = RequestMethod.GET)
	public String flockConfigure(
			@RequestParam(value="flockEventToken", required=false) String flockEventToken, 
			@RequestParam(value="flockValidationToken", required = false) String flockValidationToken) {
		return "Thank you for installing! :) -- Konverz";
	}
	
	@RequestMapping(value = "/notification", method = RequestMethod.POST)
	public String notify(
			@RequestParam(value="webhookId") final String webhookId, 
			@RequestBody final FlockIncomingWebhook message) throws Exception {
		
		service.handleNotification(webhookId, message);
		return "";
		
	}
	
	@RequestMapping(value = "/listen", method = RequestMethod.POST)
	public String listen(@RequestBody final JsonNode response) throws Exception {
		service.handleIncomingMessage(response);
		return "";
	}
}
