package com.konverz.human.interaction.service.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.konverz.human.interaction.service.domain.VoiceInteractionService;
import com.konverz.human.interaction.service.dto.VoiceCommandMessage;
import com.konverz.human.interaction.service.dto.VoiceMessage;

@RestController
@RequestMapping(value = "/dialogue", produces = MediaType.APPLICATION_JSON_VALUE)
public class VoiceInteractionController {

	@Autowired
	VoiceInteractionService service;
	
	@RequestMapping(value = "/publish", consumes = MediaType.APPLICATION_JSON_VALUE)
	private String publishMessagesToTopic(@RequestBody final VoiceCommandMessage message) throws Exception {
		service.publishMessagesToTopic(message);
		return "";
	}
	
	@RequestMapping(value = "/poll_replies") 
	private VoiceMessage pollMessagesFromTopic(@RequestParam(value="userId") final String userId) throws Exception {
		return service.pollMessagesForTopic(userId);
	}
}
